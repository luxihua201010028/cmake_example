#!/bin/bash
for dir in *; do
	if [ -d "$dir" ]; then
		cd "${dir}" || exit 1
		echo "current dictionary is ${dir}"
		if [ -d "build" ]; then
			cd "build" || exit 1
			make clean
			if [ "${dir}" = "example15" ]; then
				cmake .. -DMYDEBUG=ON
			elif [ "${dir}" = "example16" ]; then
				cmake .. -DWWW1_OPTION=OFF -DWWW2_OPTION=OFF
				cmake .. -DWWW1_OPTION=OFF -DWWW2_OPTION=ON
				cmake .. -DWWW1_OPTION=ON -DWWW2_OPTION=OFF
				cmake .. -DWWW1_OPTION=ON -DWWW2_OPTION=ON
			else
				echo "do nothing"
			fi
			make
			echo "now print the results of main"
			if [ "${dir}" = "example9" ] || [ "${dir}" = "example10" ]; then
				if [ -f "../lib/libtestfunc.a" ] && [ -f "../lib/libtestfunc.so" ]; then
					echo "libtestfunc generated successfully"
				else
					echo "libtestfunc generated failed"
				fi
			elif [ "${dir}" = "example15" ]; then
				../bin/main1
				../bin/main2
			elif [ -d "../bin" ]; then
				../bin/main
			else
				../main
			fi
			cd .. || exit 1
		else
			make clean
			cmake
			make
			echo "now print the results of main"
			if [ -d ".bin" ]; then
				./bin/main
			else
				./main
			fi
		fi
		cd .. || exit
	fi
done
